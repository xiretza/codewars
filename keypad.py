# https://www.codewars.com/kata/the-observed-pin/python
import itertools

keypad = [
    "123",
    "456",
    "789",
    " 0 "
]

get_pins = lambda observed: [
        ''.join(combo)
        for combo in itertools.product(*[
            {d: [d] + list(filter(
                    lambda x: x != ' ',
                    map(
                        lambda pos: keypad[pos[0]][pos[1]],
                        filter(
                            lambda pos: 0 <= pos[0] < len(keypad) and 0 <= pos[1] < len(row),
                            map(
                                lambda pos: (pos[0]+rownum, pos[1]+colnum),
                                tuple(
                                    itertools.product([-1, 0, 1], repeat=2)
                                )[1::2]
                            )
                        )
                    )
                ))
                for rownum, row in enumerate(keypad)
                for colnum, d in enumerate(row)
                if d != ' '
            }[d]
            for d in observed
        ])
    ]

print(get_pins('11'))
