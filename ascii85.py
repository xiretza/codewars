# for https://www.codewars.com/kata/ascii85-encoding-and-decoding

from itertools import chain, islice
import re

OFFSET_ENCODED = 33

def chunks(it, chunk_size, **kwargs):
    """
    break iterator it into chunks of length chunk_size
    usage: chunks(iterable, chunk_size[, padding=pad_value])
    if padding is not given, the last chunk may be shorter than than chunk_size

    >>> list(''.join(chunk) for chunk in chunks('abcdefghi', 3))
    ['abc', 'def', 'ghi']
    >>> list(chunks(range(6), 4, padding=None))
    [(0, 1, 2, 3), (4, 5, None, None)]
    >>> list(chunks(range(6), 4))
    [(0, 1, 2, 3), (4, 5)]
    """

    if 'padding' in kwargs:
        do_padding = True
        padding = kwargs['padding']
    else:
        do_padding = False

    it = iter(it)

    while True:
        out = []

        i = 0
        for i, el in enumerate(islice(it, chunk_size), 1):
            out.append(el)

        if i == 0:
            raise StopIteration

        if i < chunk_size and do_padding:
            padding_left = chunk_size - i
            for __ in range(padding_left):
                out.append(padding)

        yield tuple(out)


def join(it):
    """
    join all list elements of given iterator together
    """

    return chain.from_iterable(it)


def coefficients(base, num, min_amount=0):
    """
    returns list of coefficients of num for base (optionally zero-pads up to min_amount elements)
    result[-1] is *1

    >>> coefficients(10, 147)
    [1, 4, 7]
    >>> coefficients(10, 147, 5)
    [0, 0, 1, 4, 7]
    >>> coefficients(2, 14)
    [1, 1, 1, 0]
    >>> coefficients(2, 0)
    []
    """

    out = []
    while num > 0:
        out.insert(0, num % base)
        num //= base

    return [0] * max(min_amount - len(out), 0) + out


def polynomial(base, coefficients):
    """
    accumulates coefficients of a certain base
    coefficients[-1] is *1

    >>> polynomial(10, [1, 4, 7])
    147
    >>> polynomial(10, [0, 0, 1, 4, 7])
    147
    >>> polynomial(2, [1, 1, 1, 0])
    14
    >>> polynomial(10, [])
    0
    """

    coeff = list(coefficients)
    out = 0
    for power, num in enumerate(reversed(coeff)):
        out += num * base ** power

    return out


def transcode(data, source_base, source_size, target_base, target_size, fill):
    """transcode data (iterable of ints) using specified source and target encodings"""

    source_chunks = chunks(data, source_size)
    for chunk in source_chunks:
        to_remove = source_size - len(chunk)
        if to_remove:
            chunk = chain(chunk, [fill] * to_remove)

        yield list(coefficients(target_base, polynomial(source_base, chunk), min_amount=target_size))[:target_size - to_remove]


def encode_stream(data):
    """
    encodes stream to Ascii85 data
    data: str/iterable of characters
    """

    data = (ord(c) for c in data)

    processed_chunks = transcode(data, 256, 4, 85, 5, 0)

    for chunk in processed_chunks:
        if all(x == 0 for x in chunk) and len(chunk) == 5:
            yield 'z'
        else:
            for c in chunk:
                yield chr(c + OFFSET_ENCODED)

def decode_stream(data):
    """
    decodes stream of Ascii85 data
    data: str/iterable of characters
    """

    data = re.sub(r'\s+', '', data).replace('z', '!!!!!')
    data = (ord(c) - OFFSET_ENCODED for c in data)

    processed_chunks = transcode(data, 85, 5, 256, 4, ord('u') - OFFSET_ENCODED)

    return (chr(c) for c in join(processed_chunks))


def toAscii85(data):
    """
    convert data (in string format) to its Ascii85 representation

    >>> toAscii85('An Ascii85 test!')
    '<~6#If2F(8ou3&Mh#ATMr:~>'
    """

    return '<~{}~>'.format(''.join(encode_stream(data)))


def fromAscii85(data):
    """
    convert Ascii85 string to data (as string)

    >>> fromAscii85('<~6#If2F(8ou3&Mh#ATMr:~>')
    'An Ascii85 test!'
    """

    return ''.join(decode_stream(data[2:-2]))


if __name__ == '__main__':
    import doctest
    doctest.testmod()
