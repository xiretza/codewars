#!/usr/bin/env python

from enum import Enum
import operator
import math

"""
for https://www.codewars.com/kata/symbolic-differentiation-of-prefix-expressions
"""

class Operator(Enum):
    add = '+', operator.add, 2
    sub = '-', operator.sub, 2
    mul = '*', operator.mul, 2
    truediv = '/', operator.truediv, 2
    pow = '^', operator.pow, 2
    cos = 'cos', math.cos, 1
    sin = 'sin', math.sin, 1
    tan = 'tan', math.tan, 1
    exp = 'exp', math.exp, 1
    ln = 'ln', math.log, 1

    def __new__(cls, code, const_func, n_args):
        obj = object.__new__(cls)
        obj._value_ = code
        obj.const_func = const_func
        obj.n_args = n_args

        return obj

class ExpressionMeta(type):
    def __new__(cls, name, bases, classdict):
        for op in Operator:
            def operate(self, other=None, op=op):
                if other is None:
                    return Expression(op, self)

                if isinstance(other, int):
                    if isinstance(self, Constant):
                        return Constant(op.const_func(self.value, other))
                    other = Constant(other)

                if not isinstance(other, Expression):
                    return NotImplemented
                else:
                    return Expression(op, self, other)

            def r_oper(other, self, operate=operate):
                return operate(self, other)

            if op.n_args == 2:
                classdict['__{}__'.format(op.name)] = operate
                classdict['__r{}__'.format(op.name)] = r_oper
            else:
                classdict[op.name] = operate

        return super().__new__(cls, name, bases, classdict)


class Expression(metaclass=ExpressionMeta):
    def __init__(self, op, *args):
        if not isinstance(op, Operator):
            raise ValueError('expected Operator, got %r' % (op,))

        children = []
        for arg in args:
            if isinstance(arg, Expression):
                children.append(arg)
            elif isinstance(arg, int):
                children.append(Constant(arg))
            elif arg == 'x':
                children.append(Variable())
            else:
                raise ValueError('expected Expression, got %r' % (arg,))

        self.children = children
        self.operator = op
        self.parent = None

        if len(args) != op.n_args:
            raise ValueError('wrong number of arguments: %s expects %d, got %d' % (op, op.n_args, len(args)))

        if len(args) >= 1:
            self.lside = args[0]
        if len(args) >= 2:
            self.rside = args[1]

    def __str__(self):
        return '(%s%s)' % (self.operator.value, ''.join(' %s' % (child,) for child in self.children))

    def __neg__(self):
        return Expression(Operator.mul, Constant(-1), self)

    def diff(self):
        op = self.operator
        if op == Operator.add:
            return self.lside.diff() + self.rside.diff()
        elif op == Operator.sub:
            return self.lside.diff() - self.rside.diff()
        elif op == Operator.mul:
            if isinstance(self.rside, Variable):
                return self.lside
            return self.lside.diff() * self.rside + self.lside * self.rside.diff()
        elif op == Operator.truediv:
            return (self.lside.diff() * self.rside - self.lside * self.rside.diff()) / (self.rside ** 2)
        elif op == Operator.pow:
            if isinstance(self.rside, Constant):
                return self.rside * self.lside ** (self.rside-1)
            return self * (self.rside * self.lside.ln()).diff()
        elif op == Operator.cos:
            return self.lside.diff() * -self.lside.sin()
        elif op == Operator.sin:
            return self.lside.diff() * self.lside.cos()
        elif op == Operator.tan:
            return self.lside.diff() * (1 + self.lside.tan() ** 2)
        elif op == Operator.exp:
            return self.lside.diff() * self
        elif op == Operator.ln:
            return 1 / self.lside
        else:
            raise ValueError('derivative not supported for operation %s' % (op,))

    def simplified(self):
        s_children = []
        for child in self.children:
            s_children.append(child.simplified())

        op = self.operator
        if all(isinstance(child, Constant) for child in s_children):
            return Constant(op.const_func(*[child.value for child in s_children]))

        if op == Operator.mul:
            if any(child == 0 for child in s_children):
                return Constant(0)
            if len(s_children) == 2 and Constant(1) in s_children:
                one = s_children.index(Constant(1))
                return s_children[not one]
        elif op == Operator.add:
            if len(s_children) == 2 and Constant(0) in s_children:
                one = s_children.index(Constant(0))
                return s_children[not one]
        elif op == Operator.pow:
            if s_children[1] == 1:
                return s_children[0]


        return Expression(self.operator, *s_children)

class Constant(Expression):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return str(self.value)

    def __eq__(self, other):
        if isinstance(other, Constant):
            return self.value == other.value
        else:
            return self.value == other

    def __neg__(self):
        return Constant(-self.value)

    def diff(self):
        return Constant(0)

    def simplified(self):
        return self

class Variable(Expression):
    def __init__(self, name):
        if not isinstance(name, str) and len(name) == 1:
            raise ValueError('expected character, got %r' % (name,))

        self.name = name

    def __str__(self):
        return self.name

    def diff(self):
        return Constant(1)

    def simplified(self):
        return self

def parse(parts, offset=0):
    ident = parts[offset]
    if ident == '(':
        op = Operator(parts[offset+1])
        pos = offset + 2
        exprs = []
        while True:
            if len(parts) <= pos:
                raise ValueError('unexpected end of equation')

            if parts[pos] == ')':
                pos += 1
                break

            expr, pos = parse(parts, pos)
            exprs.append(expr)

        return Expression(op, *exprs), pos
    else:
        if ident == 'x':
            return Variable(ident), offset+1
        else:
            return Constant(int(ident)), offset+1

def diff(s):
    parts = s.translate(str.maketrans({'(': '( ', ')': ' )'})).split()
    exp, pos = parse(parts)
    return str(exp.diff().simplified())

print(diff('(cos x)'))
print(Expression(Operator.pow, Variable('x'), Constant(1)).simplified())
