# https://www.codewars.com/kata/game-of-go/train/python
from enum import Enum
import string
import re
import copy

class Player(Enum):
    white = 'o'
    black = 'x'

class Go:
    w_coords = 'ABCDEFGHJKLMNOPQRSTUVWXYZ'

    def __init__(self, height, width=None):
        if not width:
            width = height

        if width > 25 or height > 25:
            raise ValueError('maximum board size is 25x25, got %dx%d' % (height, width))

        self.height = height
        self.width = width
        self.reset()

    def __str__(self):
        return '  ' + ' '.join(self.w_coords[:self.width]) + '\n' + '\n'.join(
                str(rownum) + ' ' + ' '.join(row) for rownum, row in zip(range(self.height, 0, -1), self.board)
            )

    def position_to_coord(self, pos):
        """
        returns (x, y) tuple from string representation (like '3A')
        does bounds and validity checking, throws error if fails
        """

        m = re.match('([0-9]+)([A-Z]+)', pos)
        if not m:
            raise ValueError('bad position: {}'.format(pos))

        x = self.height - int(m.group(1))

        if m.group(2) not in self.w_coords:
            raise ValueError('bad position (bad y coordinate): {}'.format(w))
        y = self.w_coords.index(m.group(2))

        if not (0 <= x < self.height and 0 <= y < self.width):
            raise ValueError('position out of bounds: {}'.format(pos))

        return (x, y)

    @property
    def turn(self):
        return self._turn.value

    def _change_turn(self):
        """changes the internal turn variable to the other player"""

        if self._turn == Player.black:
            self._turn = Player.white
        else:
            self._turn = Player.black

    def reset(self):
        self._turn = Player.black
        self.prev_states = []

        self.board = [['.'] * self.width for i in range(self.height)]

    def get_coord(self, coord, y=None):
        """
        takes either (x, y) tuple or x, y
        x: row (from top)
        y: column (from left)
        """

        if y and isinstance(coord, int):
            coord = (coord, y)
        elif not (isinstance(coord, tuple) and not y):
            raise ValueError('invalid coord: %r' % (coord,))

        return self.board[coord[0]][coord[1]]

    def get_position(self, pos):
        return self.get_coord(self.position_to_coord(pos))

    def push_state(self):
        state = {'turn': self.turn, 'board': copy.deepcopy(self.board)}
        self.prev_states.append(state)

    def rollback(self, amount=1):
        if amount > len(self.prev_states):
            raise ValueError('tried to roll back too far')

        state = self.prev_states.pop(-amount)
        if amount > 1:
            del self.prev_states[-amount+1:]

        self._turn = Player(state['turn'])
        self.board = state['board']

    def move(self, *positions):
        if len(positions) == 0:
            raise ValueError('empty move set: {}'.format(positions))

        for pos in positions:
            coord = self.position_to_coord(pos)
            if self.get_coord(coord) != '.':
                raise ValueError('field is already occupied: {}'.format(pos))

            self.push_state()
            self.board[coord[0]][coord[1]] = self._turn.value
            self._change_turn()


    def get_size(self):
        return {'height': self.height, 'width': self.width}

game = Go(9)
game.move('9A', '9B')
print(game)
game.move('5C', '4J')
print(game)
game.rollback()
print(game)
game.move('4J')
print(game)
