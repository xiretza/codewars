# https://www.codewars.com/kata/5518a860a73e708c0a000027/train/python
def last_digit(lst):
    if not lst:
        return 0

    moduli = []
    # build moduli for each level
    for e in lst:
        num_prev_mods = len(moduli[-1])-1 if moduli else 10
        print('%d^e mod %d' % (e, num_prev_mods))
        powers = [1 % num_prev_mods]
        #powers = [1]
        while True:
            d = 1 + (powers[-1] * e - 1) % num_prev_mods
            if d in powers[1:]:
                break
            else:
                powers.append(d)

        print(powers)
        moduli.append(powers)

    print('moduli : %s' % (moduli,))

    res = lst[-1] % len(moduli[-1])
    for level, num in reversed(list(enumerate(lst[:-1]))):
        print('%d: %d^%d mod %d = %d' % (level, num, res, len(moduli[level-1])-1 if level > 0 else 10, moduli[level][res]))
        res = moduli[level][res]

    return res


tests = (
    ((7,6,21), 1),
    ((3,4,5), 1),
    ((4,3,6), 4),
)

for test in tests:
    print('%s = %s (should be %s)' % (test[0], last_digit(test[0]), test[1]))
